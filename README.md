# Cloud-Comp
This is the coursework i have created for cloud computing,
Unfortunately, after 30+ hours and months of work i cant get the system to work as intended. i have completed all the tutorials more than once and gone through every single slide as well as attending every lecture/workshop that was held.
The code i have here is my final attempt of trying to get the week 5 and week3 and week1 code to combine to create a working cluster that will use zeroMQ to communicate.
As i have completed all the lectures and tutorials i understand exactly what i couldnt complete and wish to still complete in the future. I would have firstly got the messaging tier to work as intended so all nodes can communicate they are alive. i would then have got the leadership election working between the nodes so that their is a leader that can use the docker API to create any depriciated nodes. then i would have added a mongoDB cluster that can connect as the database tier with a leader being allected so it can be written to by one node which will then create replica data which would have passed onto the other nodes in the cluster. finally i would have included an auto scaling feature which could have been used to scale the system larger at specific times and smaller when it was expected to be less busy. I finally would have also used the autoscaling feature to implement a scalability from the CPU stress/load and scale larger if the CPU is under more stress.

i understand that the work i have created is not up to a good standard but i have tried for months to get a working system but, even though i knew exactly what i had to implement, i couldnt get any of the features to work after months of trial and error. to see my git logs please use the following URL to access it to view the work i have tried to add. I found the theory of this module good and feel i have learned a lot but the practical side of the module i couldnt get a grasp of and could never get working past the tutorials (which i had working all successfully). 

https://gitlab.com/LilLuke98/cloud-comp.git

to try to get the system to work please change the IP addresses accordingly to yours so it will connect with a simple sudo docker-compose up command.

